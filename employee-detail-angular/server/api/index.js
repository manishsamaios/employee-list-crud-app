var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");

var mongoClient = require("mongodb").MongoClient;

var app = express();
app.use(cors());
app.use(bodyParser());


app.get('/employees/', function(req, res){
    mongoClient.connect("mongodb://localhost:27017/EmployeeDB", function(err, db){
        if (!err) {
            db.collection('employees').find().toArray(function(err, docs){
                this.employees = docs;
                res.send(JSON.stringify(employees));
            });
        }
    });
});

app.get('/employees/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/EmployeeDB', function(err, db){
        if (!err) {
            var id = Number(req.params.id);
            db.collection('employees').findOne({id: id}, function(err, employee){
                resp.send(JSON.stringify(employee));
            });
        }
    });
});

app.post('/employees', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/EmployeeDB', function(err, db){
        if (!err) {
            var employee = req.body;
            db.collection('employees').insertOne(employee, function(err, result){
                resp.send('Added new employee');
            });
        }
    });
});

app.put('/employees/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/EmployeeDB', function(err, db){
        if (!err) {
            var id = Number(req.params.id);
            var newvalues = { $set: {name: req.body.name, role: req.body.role, salary: Number(req.body.salary) } };
            db.collection('employees').updateOne({id: id}, newvalues, function(err, result){
                resp.send({msg: 'Employee updated successfully'});
            });
        }
    });
});

app.delete('/employees/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/EmployeeDB', function(err, db){
        if (!err) {
            var id = Number(req.params.id);
            db.collection("employees").remove({id:id}, function(err, response){
                resp.send("row deleted scucessfully");
            });
        }
    });
});

app.listen(9000, () => console.log("API started listening"));