export class Employee {
    id: number;
    name: string;
    role: string;
    salary: number;

    constructor(employeeId?: number, employeeName?: string, employeeRole?: string, employeeSalary?: number) {
        this.id = employeeId;
        this.name = employeeName;
        this.role = employeeRole;
        this.salary = employeeSalary;
    }


}
