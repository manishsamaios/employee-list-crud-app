import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-employee-crud',
  templateUrl: './employee-crud.component.html',
  styleUrls: ['./employee-crud.component.css']
})
export class EmployeeCrudComponent implements OnInit {

  emp: Employee = new Employee();
  url: string = 'http://localhost:9000/employees'

  constructor(private _http: HttpClient) { }

  createNewEmployee() {
    this._http.post(this.url, this.emp,{
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }).subscribe(x => console.log(x));
    this.emp = new Employee();
    alert("Employee added successfully");
  }

  retrieveEmployee(id: number) {
    var updatedUrl = `${this.url}/${id}`;
    this._http.get<Employee>(updatedUrl)
      .subscribe(x => (this.emp = x));
  }

  updateEmployee(id: number) {
    var updatedUrl = `${this.url}/${id}`;
    this._http.put(updatedUrl, this.emp,{
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }).subscribe(x => console.log(x));
    this.emp = new Employee();
    alert("Employee updated successfully");
  }

  deleteEmployee(id: number) {
    var updatedUrl = `${this.url}/${id}`;
    this._http.delete(updatedUrl).subscribe(x => console.log(x));
    alert("Employee removed successfully");
  }

  ngOnInit() {
  }

}
