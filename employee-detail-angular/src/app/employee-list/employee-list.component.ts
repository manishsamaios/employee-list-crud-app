import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees = new Array<Employee>();
  url:string = 'http://localhost:9000/employees';
  emp:Employee = new Employee();

  constructor(private _http: HttpClient) { 
    this._http.get<Employee[]>(this.url)
      .subscribe(x => (this.employees = x));
  }

  getEmployeeForIndex(index) {
    this.emp = JSON.parse(JSON.stringify(this.employees[index]));
  }

  ngOnInit() {
  }

}
